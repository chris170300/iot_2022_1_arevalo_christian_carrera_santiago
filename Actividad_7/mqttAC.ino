#include <WiFi.h>
#include <PubSubClient.h>

///SENSOR 1///

const int triger = 2;
const int eco = 4;
char dato1[40];
int distancia;
int duracion;

///SENSOR 2///

const int triger2 = 15;
const int eco2 = 5;
char dato2[40];
int distancia2;
int duracion2;

WiFiClient esp32Client;
PubSubClient mqttClient(esp32Client);

const char* ssid     = "Carrera";
const char* password = "john98385692";

 ///DIRECCION DE SERVIDOR
 
char *server = "broker.emqx.io";
int port = 1883;



///FUNCION PARA CONECTAR CON LA RED WI FI
void wifiInit() {
    Serial.print("Conectándose a ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
        delay(500);  
    }
    Serial.println("");
    Serial.println("Conectado a WiFi");
    Serial.println("Dirección IP: ");
    Serial.println(WiFi.localIP());
  }

///FUNCION QUE SE EJECUTARA AL RECIBIR INFORMACION DEL TOPICO
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Mensaje recibido [");
  Serial.print(topic);
  Serial.print("] ");
  ///DE STRING A INT
  char payload_string[length + 1];
  
  int var = 0;
  int resultI;

  memcpy(payload_string, payload, length);
  payload_string[length] = '\0';
  resultI = atoi(payload_string);
  
  var = resultI;

}



void reconnect() {
  while (!mqttClient.connected()) {
    Serial.print("Conectandose al servidor MQTT...");

    if (mqttClient.connect("arduinoClient")) {
      Serial.println("Conectado");

      
      
    } else {
      Serial.print("Fallo");
      Serial.println(" Reconectando en 5 segundos");
      delay(5000);
    }
  }
}

void setup()
{
  
  Serial.begin(115200);
  pinMode(triger,OUTPUT);
  pinMode(eco,INPUT);
  pinMode(triger2,OUTPUT);
  pinMode(eco2,INPUT);
  delay(10);
  wifiInit();
  mqttClient.setServer(server, port);///Asignamos al objeto mqttClient el servidor y el puerto que declaramos
  mqttClient.setCallback(callback);
}

void loop()
{
   if (!mqttClient.connected()) {
    reconnect();
  }

///CALIBRACION SENSOR 1
digitalWrite(triger,LOW);
delayMicroseconds(4);
digitalWrite(triger,HIGH);
delayMicroseconds(10);
digitalWrite(triger,LOW);
duracion=pulseIn(eco,HIGH);
duracion=duracion/2;
distancia=duracion/29.2;
Serial.print("sensor1: ");
Serial.println(distancia);

///IMPRESION DE DATOS EN EL TOPIC SENSOR1

sprintf(dato1, "Valor sensor1: %d ", distancia);
mqttClient.publish("Sensor1",dato1);
delay(2000);

///CALIBRACION SENSOR 2

digitalWrite(triger2,LOW);
delayMicroseconds(4);
digitalWrite(triger2,HIGH);
delayMicroseconds(10);
digitalWrite(triger2,LOW);
duracion2=pulseIn(eco2,HIGH);
duracion2=duracion2/2;
distancia2=duracion2/29.2;
Serial.print("sensor2: ");
Serial.println(distancia2);

///IMPRESION DE DATOS EN EL TOPIC SENSOR2

sprintf(dato2, "Valor sensor2: %d ", distancia2);
mqttClient.publish("Sensor2",dato2);
delay(3000);


}
